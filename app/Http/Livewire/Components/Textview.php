<?php

namespace App\Http\Livewire\Components;

use Livewire\Component;

class Textview extends Component
{
    public $label;
    public $variable;


    public function changeVariable($value){

        $this->variable = $value;

    }


    public function render()
    {
        return view('livewire.components.textview');
    }
}
