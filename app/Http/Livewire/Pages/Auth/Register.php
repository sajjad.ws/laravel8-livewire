<?php

namespace App\Http\Livewire\Pages\Auth;

use App\Models\User;
use Livewire\Component;

class Register extends Component
{



    public User $user;

    protected $rules = [
        'user.name' => 'required|min:8',
        'user.email' => 'required|email|unique:users,email',
        'user.password' => 'required|min:8'
    ];

    public function mount(){
        $this->user = new User();
    }

    public function updated($email){

     /*   $this->validateOnly($email);
*/

    }

    public function register()
    {
        $this->validate();

        $this->user->password = bcrypt($this->user->password);

        $this->user->save();

        return redirect()->to('/home');
    }


    public function render()
    {
        return view('livewire.pages.auth.register')->layout('master');
    }
}
