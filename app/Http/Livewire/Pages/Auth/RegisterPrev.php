<?php

namespace App\Http\Livewire\Pages\Auth;

use App\Models\User;
use Livewire\Component;

class RegisterPrev extends Component
{


    public $name;
    public $email;
    public $password;

    public User $users;

    protected $rules = [
        'name' => 'required|min:8',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:8'
    ];

    public function updated($email){

        $this->validateOnly($email);


    }

    public function register()
    {
        $this->validate();
        dd($this->name);
    }


    public function render()
    {
        return view('livewire.pages.auth.register')->layout('master');
    }
}
