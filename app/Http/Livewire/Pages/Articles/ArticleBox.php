<?php

namespace App\Http\Livewire\Pages\Articles;

use App\Models\Article;
use Livewire\Component;

class ArticleBox extends Component
{

    public Article $article;




    public function like(){


        $this->article->increment('likes');

    }

    public function render()
    {
        return view('livewire.pages.articles.article-box');
    }
}
