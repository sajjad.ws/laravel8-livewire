<?php

namespace App\Http\Livewire\Pages\Articles;

use App\Models\Article;
use Livewire\Component;

class Index extends Component
{



    public function render()
    {
        return view('livewire.pages.articles.index',[
            'articles'=>Article::latest()->paginate(5)
        ])->layout('master');
    }
}
