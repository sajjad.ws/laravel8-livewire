<?php

namespace App\Http\Livewire\Pages;

use Livewire\Component;

class Home extends Component
{

    public $a = 0;

    public function increment(){

        $this->a += 1;
    }

    public function getBProperty()
    {
        return $this->a+1;
    }

    public function render()
    {
        return view('livewire.pages.home')->layout('master');
    }
}
