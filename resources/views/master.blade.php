<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My LiveWire WebSite</title>

    @livewireStyles
    <link rel="stylesheet" href="{{mix('/css/app.css')}}">

</head>
<body>

<livewire:layout.header article="sadasd">
    {{$slot}}
</livewire:layout.header>


    @livewireScripts
</body>
</html>
