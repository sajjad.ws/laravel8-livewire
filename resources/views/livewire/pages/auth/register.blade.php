<div>
    <div class="container mt-5">
        <form wire:submit.prevent="register">
            @csrf
            <div class="row mb-3">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" wire:model="user.name" >
                    @error('user.name') <span> {{$message}}</span> @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" wire:model="user.email">
                    @if($errors->has('user.email'))
                        <span> {{$errors->first('user.email')}}</span>
                    @endif
                </div>
            </div>
            <div class="row mb-3">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3"  wire:model="user.password">
                    @if($errors->has('user.password'))
                        <span> {{$errors->first('user.password')}}</span>
                    @endif
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Sign in</button>
        </form>
    </div>
</div>
