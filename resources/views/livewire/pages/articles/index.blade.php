<div>
    <div class="container">
    <div class="row">
        <div class="col-12">
    @foreach($articles as $article)

        <livewire:pages.articles.article-box :article="$article"/>

    @endforeach
        </div>
    </div>
    </div>
</div>
