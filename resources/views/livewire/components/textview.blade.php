<div>
    <div class="row mb-3">
        <label for="inputEmail3" class="col-sm-2 col-form-label" >@if($label)
                {{$label}}
            @else
                                                                      بدون نام
            @endif
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputEmail3" wire:model="variable">
        </div>
    </div>
</div>
