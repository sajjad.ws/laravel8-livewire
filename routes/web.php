<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

   auth()->loginUsingId(1,true);
    /* session()->save();*/
    //request()->session()->pull('username');
    //Auth::loginUsingId(1, $remember = true);
    //dd(auth()->check());
    dd(auth()->check());
    $article = [
        'title' => 'onvan',
        'body'=>'badane'
    ];



    return view('master', compact('article'));
});


/*
Route::get('/test-gate', function () {
    Auth::loginUsingId(1,true);

    dd(Auth::viaRemember());
    if(Gate::allows('edit-user')){
        dd('yes');
    }
    dd('no');

    return view('test-gates',);
});*/

Route::get('articles',\App\Http\Livewire\Pages\Articles\Index::class);
Route::get('register',\App\Http\Livewire\Pages\Auth\Register::class);
Route::get('home',\App\Http\Livewire\Pages\Home::class);

